<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TaskRepository $taskRepository
    ) {
    }

    #[Route(path: '/tasks/todo', name: 'task_list')]   
    public function listTasks(): ?Response
    {
        return $this->render('task/list.html.twig', [
            'tasks' => $this->taskRepository->findBy(['isDone' => false]),
            ]);
    }

    #[Route('/tasks/done', name: 'task_list_done')]
    public function listTasksDone(): Response
    {
        return $this->render('task/list.html.twig', [
            'tasks' => $this->taskRepository->findBy(['isDone' => true]),
        ]);
    }

    #[Route(path: '/tasks/create', name: 'task_create')]
    public function createTask(Request $request, UserInterface $user = null): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);

        // Handle the form submission
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task->setCreatedAt(new \DateTimeImmutable())
                ->toggle(false);
            if ($user instanceof User) {
                $task->setUser($user);
            }

            // Check if the user is an instance of User
            if (!$user instanceof User) {
                throw new \LogicException();
            }
            $this->taskRepository->save($task, true);

            $this->addFlash('success', 'La tâche a été bien été ajoutée.');

            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/tasks/{id}/edit', name: 'task_edit')]
    public function editTask(Task $task, Request $request): Response
    {
        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task->setUpdatedAt(new \DateTimeImmutable());
            $this->entityManager->flush();

            $this->addFlash('success', 'La tâche a bien été modifiée.');

            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/edit.html.twig', [
            'form' => $form->createView(),
            'task' => $task,
        ]);
    }

    #[Route(path: '/tasks/{id}/toggle', name: 'task_toggle')]
    public function toggleTask(Task $task): Response
    {
        $this->checkUserRightsOnTask($task);
        // check if the user has the right to toggle the task
        $task->toggle(!$task->isDone());
        $this->entityManager->flush();

        $this->addFlash(
            'success',
            sprintf(
                'La tâche %s a bien été marquée comme %s.',
                $task->getTitle(),
                $task->isDone() ? 'faite' : 'non terminée'
            )
        );

        return $this->redirectToRoute($task->isDone() ? 'task_list' : 'task_list_done');
    }

    #[Route(path: '/tasks/{id}/delete', name: 'task_delete')]
    public function deleteTask(Task $task): Response
    {
        // Check if the user has the right to delete the task
        $this->checkUserRightsOnTask($task);

        // Delete the task
        $this->entityManager->remove($task);
        $this->entityManager->flush();

        // Add a flash message to confirm the deletion
        $this->addFlash('success', 'La tâche a bien été supprimée.');

        return $this->redirectToRoute('task_list');
    }

    private function checkUserRightsOnTask(Task $task): void
    {
        // If the task is anonymous, only an admin can delete it
        if ('anonyme' === $task->getUser()->getUsername()) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN');

            return;
        }
        // If the task is not anonymous, only the user who created it can delete it
        if ($task->getUser() !== $this->getUser()) {
            throw new AccessDeniedHttpException();
        }
    }

}
