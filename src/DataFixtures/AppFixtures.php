<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');


        $users = [
            [
                'username' => 'admin',
                'email' => 'admin@example.com',
                'roles' => ['ROLE_ADMIN']
            ],
            [
                'username' => 'user1',
                'email' => 'user1@example.com',
                'roles' => ['ROLE_USER']
            ],
            [
                'username' => 'user2',
                'email' => 'user2@example.com',
                'roles' => ['ROLE_USER']
            ], [
                'username' => 'anonyme',
                'email' => 'anonyme@example.com',
                'roles' => ['ROLE_USER']
            ]
        ];

        foreach ($users as $user) {
            $newUser = new User();
            $newUser->setUsername($user['username'])
                ->setEmail($user['email'])
                ->setRoles($user['roles'])
                ->setCreatedAt(new DateTimeImmutable())
                ->setPassword('$2y$13$FXWFBi6c9oYddNsnioKfwuUClMOTCPaOt0.P9X4o0vxm2Pe9fNVEG');
            $manager->persist($newUser);
            for ($i = 0; $i < 25; $i++) {
                $task = new Task();
                $task->setTitle($faker->words(3, true))
                    ->setContent($faker->paragraph())
                    ->setCreatedAt(new DateTimeImmutable())
                    ->setUser($newUser)
                    ->toggle($faker->boolean());

                $manager->persist($task);
            }

        }
        $manager->flush();
    }
}
