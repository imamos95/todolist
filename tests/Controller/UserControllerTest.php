<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserControllerTest extends WebTestCase
{
    private KernelBrowser $client;


    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->urlGenerator = $this->client->getContainer()->get('router.default');
        $this->userRepository = $this->client->getContainer()->get('doctrine')->getManager()->getRepository(User::class);
    }

    public static function setUpBeforeClass(): void
    {
        exec('php bin/console d:f:l --env=test --no-interaction --quiet');
    }

    public function testPermissionsOnUserPages(string $route, array $params = []): void
    {
        // As a visitor, test redirect to login page for current route
        $expectedRedirect = $this->urlGenerator->generate('login', referenceType: UrlGeneratorInterface::ABSOLUTE_URL);
        $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate($route, $params));
        $this->assertResponseRedirects($expectedRedirect);

        // Log-in non-admin user and check for 403 response
        $regularUser = $this->getUser('user1');
        $this->client->loginUser($regularUser);
        $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate($route, $params));
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        // Log-in admin user and check for 200 response
        $adminUser = $this->getUser('admin');
        $this->client->loginUser($adminUser);
        $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate($route, $params));
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function usersRouteDataProvider(): array
    {
        return [
            ['user_list'],
            ['user_create'],
            ['user_edit', ['id' => 1]],
        ];
    }

    public function testCreateUser(): void
    {
        $adminUser = $this->getUser('admin');
        $this->client->loginUser($adminUser);
        $crawler = $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate('user_create'));

        // Test the create user form as admin
        $form = $crawler->selectButton('Ajouter')->form();
        $form['user[username]'] = 'TestAddUsername';
        $form['user[password][first]'] = 'TestAddUserPassword';
        $form['user[password][second]'] = 'TestAddUserPassword';
        $form['user[email]'] = 'TestAddUserMail@test.tst';
        $this->client->submit($form);

        // Check redirects on form submission and value of front-end flash message
        $this->assertResponseRedirects($this->urlGenerator->generate('user_list'));
        $this->client->followRedirect();
        $this->assertSelectorTextContains('div.alert.alert-success', "L'utilisateur a bien été ajouté.");

        // Finally, check that the user has been created in the test database
        $createdUser = $this->userRepository->findBy(['username' => 'TestAddUsername']);
        $this->assertNotEmpty($createdUser);
        $this->assertEquals('TestAddUserMail@test.tst', $createdUser[0]->getEmail());
    }

    public function testEditUser(): void
    {
        /** @var User $user2 */
        $adminUser = $this->getUser('admin');
        $user2 = $this->getUser('user2');

        $this->client->loginUser($adminUser);
        $crawler = $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate(
            'user_edit',
            ['id' => $user2->getId()]
        ));

        $this->assertFalse(in_array('ROLE_ADMIN', $user2->getRoles()));

        // Test the edit user form as admin : editing user_2, adding admin role
        $form = $crawler->selectButton('Modifier')->form();
        $form['user[username]'] = $user2->getUsername();
        $form['user[password][first]'] = $user2->getPassword();
        $form['user[password][second]'] = $user2->getPassword();
        $form['user[email]'] = $user2->getEmail();
        $form['user[is_admin]'] = true;
        $this->client->submit($form);

        // Check redirects on form submission and value of front-end flash message
        $this->assertResponseRedirects($this->urlGenerator->generate('user_list'));
        $this->client->followRedirect();
        $this->assertSelectorTextContains('div.alert.alert-success', "L'utilisateur a bien été modifié");

        // Finally, check that the user has been updated in the test database
        $updatedUser = $this->userRepository->find($user2->getId());
        $this->assertTrue(in_array('ROLE_ADMIN', $updatedUser->getRoles()));
    }

    private function getUser(string $name): ?User
    {
        $entityManager = $this->client->getContainer()->get('doctrine.orm.entity_manager');
        /**
         * @var UserRepository $userRepository
         */
        $userRepository = $entityManager->getRepository(User::class);
        return $userRepository->findOneBy(['username' => $name]);
    }

}
