<?php

namespace App\Tests\Form;

use App\Form\TaskType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Test\TypeTestCase;

class TaskTypeTest extends TypeTestCase
{
    public function testBuildForm()
    {
        // Créez une instance de votre classe TaskType
        $taskType = new TaskType();

        // Construisez le formulaire
        $form = $this->factory->create(TaskType::class);

        // Effectuez des assertions pour vérifier le comportement attendu
        $this->assertTrue($form->has('title'));
        $this->assertTrue($form->has('content'));
        $this->assertInstanceOf(TextareaType::class, $form->get('content')->getConfig()->getType()->getInnerType());


        // Par exemple, pour tester les données soumises au formulaire
        $formData = [
            'title' => 'Titre de la tâche',
            'content' => 'Contenu de la tâche',
        ];

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($formData, $form->getData());


        // Par exemple, si vous avez une contrainte NotBlank pour le champ 'title'
        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }

    }

}
