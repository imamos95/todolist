<?php

namespace App\Tests\Form;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Form\Test\TypeTestCase;

class UserTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'username' => 'john_doe',
            'password' => [
                'first' => 'password123',
                'second' => 'password123',
            ],
            'email' => 'john@example.com',
            'is_admin' => true,
        ];

        $form = $this->factory->create(UserType::class);

        $expectedEntity = new User(); // Replace with your actual User entity class

        $form->submit($formData);

        // compare the form data to the expected data
        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($expectedEntity, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function testSubmitInvalidData()
    {
        $user = new User();

        $form = $this->factory->create(UserType::class, $user);

        $formData = [
            'username' => '', // Empty username
            'password' => [
                'first' => 'shortPwd',
                'second' => 'differentPwd', // Passwords don't match
            ],
            'email' => 'invalidEmail', // Invalid email format
        ];

        $form->submit($formData);

        self::assertTrue($form->isSubmitted());
        self::assertFalse($form->isValid());

        // Check for specific error messages in form fields
        self::assertStringContainsString('NotBlank', $form->get('username')->getErrors()->__toString());
        self::assertStringContainsString('Invalid password', $form->get('password')->getErrors()->__toString());
        self::assertStringContainsString('Invalid email', $form->get('email')->getErrors()->__toString());
    }


}
