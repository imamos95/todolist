<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    private Task $task;

    public function setUp(): void
    {
        $this->task = new Task();
    }

    public function testGetId(): void
    {
        $this->assertNull($this->task->getId());
    }

    public function testCreatedAt(): void
    {
        $createdAt = new DateTimeImmutable();
        $this->task->setCreatedAt($createdAt);
        $this->assertEquals($createdAt, $this->task->getCreatedAt());
    }

    public function testUpdatedAt(): void
    {
        $updatedAt = new DateTimeImmutable();
        $this->task->setUpdatedAt($updatedAt);
        $this->assertEquals($updatedAt, $this->task->getUpdatedAt());
    }

    public function testTitle(): void
    {
        $title = 'Test Task';
        $this->task->setTitle($title);
        $this->assertEquals($title, $this->task->getTitle());
    }

    public function testIsDone(): void
    {
        $this->assertFalse($this->task->isDone());
        $this->task->toggle(true);
        $this->assertTrue($this->task->isDone());
    }

    public function testUser(): void
    {
        $user = new User();
        $this->task->setUser($user);
        $this->assertSame($user, $this->task->getUser());
        $this->assertNull(null, $this->task->getUser()->getTasks());
    }

    public function testToggle()
    {
        $task = $this->getTaskEntity();
        $this->assertSame(false, $task->isDone());

        $task->toggle(true);
        $this->assertSame(true, $task->isDone());
    }

    private function getTaskEntity(): Task
    {
        return (new Task())
            ->setTitle('Test Title')
            ->setContent('Test Content')
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUser(new User())
            ->setIsDone(false);
    }

}
