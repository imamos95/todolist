<?php

namespace App\Tests\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginAuthenticatorTest extends WebTestCase
{
    public function testAuthenticationSuccess()
    {
        $client = static::createClient();

        $client->request('POST', '/login', [
            'username' => 'user1',
            'password' => 'password',
        ]);

        $this->assertInstanceOf(RedirectResponse::class, $client->getResponse());

        $this->assertEquals('/', $client->getResponse()->getTargetUrl());
    }

    public function testAuthenticationFailure()
    {
        $client = static::createClient();

        // Make a request with invalid credentials
        $client->request('POST', '/login', [
            'username' => 'invalid_username',
            'password' => 'invalid_password',
        ]);

        // Assert that the response is not a redirect (authentication failure)
        $this->assertNotInstanceOf(RedirectResponse::class, $client->getResponse());

        // Add additional assertions based on your authentication failure behavior
        // For example, you might assert that the response contains a specific error message.
        $this->assertContains('Invalid credentials', $client->getResponse()->getContent());
    }
}